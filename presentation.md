title: BuildStream and&nbsp;Bazel
class: animation-fade
layout: true

<!-- This slide serves as the base layout for all the slides -->
.bottom-bar[
  <img height="20px" width="20px" src="assets/codethink-o-white.svg" />
  {{title}}
]

---
class: impact

# {{title}}
## Daniel Silverstone<br>`daniel.silverstone@codethink.co.uk`

???

* Placeholder slide
* Here to act as timer-start
* Advance to next (identical slide) to begin timer.

---
count: false
class: impact

# {{title}}
## Daniel Silverstone<br>`daniel.silverstone@codethink.co.uk`

???

Say "Hi and welcome"

* This might be a little different…
* …but I hope you'll find it interesting…
* …informative and useful

---
class: playful

# Who am I?

???

* Daniel Silverstone
* Worked on building platforms for 20 years
* Debian, user, then developer
* Internal platforms, based on Debian/RedHat, then Poky, then rolled our own
* Finally ending up working on/with BuildStream at Codethink
* And in the past year or so, on making BuildStream work well in a Bazel
  oriented ecosystem

---
class: playful

# Why am I here?

???

* In the past, people who know about both Bazel and BuildStream have asked
   "Why is A better than B?" one way or the other.
* For quite a long while now, we've been saying "How can they work together
   to be better than their parts?"
* Not about Bazel vs. BuildStream.
* *Is* about Bazel *and* BuildStream working together
* …to solve complex systems integration problems

---
class: impact

# Bazel

???

* To give some context…
* …from my perspective
* …Bazel is a build tool
* …which tightly integrates software components
* …it is rapid, does its best to be as correct as possible
* …has excellent caching and remote build support
* …and has an active and interested community

<!-- -->

* We at Codethink have been working on…
* …getting projects we're involved with to be
* …more aligned with Bazel
* …and able to use Bazel related technologies

<!-- -->

* To solve the complex problems of **systems** integration

---

# Systems integration

???

* Here to talk about systems integration
* Need to define that for the purposes of this talk.

---
count: false

# Systems integration

* Basic platform software

???

* Any system needs basic platform
* Codethink tends to deal with Linux, hence focus today
* Same applies to complex systems on Windows/MacOS
* For now, platform means:
    * Kernel
    * libc
    * Low level userland
    * Systemd, coreutils, that kind of thing

---
count: false

# Systems integration

* Basic platform software
* Toolchains

???

* To get anywhere, you need a toolchain
* Usually acquire one from elsewhere
* Often then bootstrap one you can trust more
* So this point adds in one or more compilers/languages + runtimes

---
count: false

# Systems integration

* Basic platform software
* Toolchains
* Middleware

???

* Middleware always needed in complex systems
* Maybe tensorflow, gstreamer, etc.
* Maybe more domain specific like automotive comms
* Maybe X11 or wayland

---
count: false

# Systems integration

* Basic platform software
* Toolchains
* Middleware
* Applications

???

* Once you have all that in place…
* you need to integrate **your** applications onto the system.
* This likely includes a number of other libraries
* Also system configuration changes

---
count: false

# Systems integration

* Basic platform software
* Toolchains
* Middleware
* Applications
* Deploy it

???

* Finally you have to deploy - often forgotten in this game.
* Maybe this is building docker images
* Maybe disk images for the factory
* Maybe OTA artifacts for updating already installed systems
* Or maybe just writing it to an AWS lambda
* Most likely, some combination of all these and more.

---

# An example integration

???

* This work leads to:
    * Interesting problems
    * Requiring particular approaches
    * In order to minimise the cost to integrators
    * Without making developers' lives worse
* So let's explore this a little more…

---
count: false

# An example integration

* A small system platform…

???

* Circa 500 projects
* Have to work in concert
* Different build systems
* Complex dependency chains

---
count: false

# An example integration

* A small system platform…
* …which changes and evolves over time

???

* Each project has its own cadence
    * The software itself may change
    * Dependencies may come and go
    * Changes in dependencies may require updates in dependents
* These changes need to be managed effectively in order to cope

---
count: false

# An example integration

* A small system platform…
* …which changes and evolves over time
* …which needs to be reliably reproduced for a product's lifetime

???

* Lifetime often years
* Automotive lifetime can be decades
* Integrated systems must be:
    * Updatable
    * Rebuildable across the lifetime
    * Cope with security updates
    * Cope with changes in customer expectations
* Also developers come and go
    * Working environments must also be rebuildable
    * Cope with changes in developer hardware
    * etc.

---
count: false

# An example integration

* A small system platform…
* …which changes and evolves over time
* …which needs to be reliably reproduced for a product's lifetime
* …which has to be maintained with minimal cost and friction

???

* All this needs to be done with:
    * Minimal wastage of time
    * Minimal effort
    * Minimal friction
    * All that is just "minimal cost"
* Integrators need:
    * Tooling to help them do their job
    * Automation to help them not have to do their job
* Developers need:
    * Ease of developing easily integrated work
    * Not have to care about integrator tooling
    * Except if it can improve their lives too
* Eliminate the "works for me" response to issue reports

---

# Scaling up - how many teams?

???

* Large product development often involves many engineering teams
* Often distributed among companies
* With complex interdependencies
* Let's do a basic breakdown into three areas…

---
count: false

# Scaling up - how many teams?

.col-4[
### Platform

* Toolchains
* Base OS
* Various middlewares
* Core UX facilities
* OTA updates
* …etc…
]

???

* Platforms need (potentially) many teams to develop
* All these teams feed into each other
* Together they produce:
    * Target base platform
    * Development platform
    * Toolchains for Bazel and other build systems
    * etc.
* Then consumed by…

---
count: false

# Scaling up - how many teams?

.col-4[
### Platform

* Toolchains
* Base OS
* Various middlewares
* Core UX facilities
* OTA updates
* …etc…
]

.col-4[
### Apps

* Consume platforms and toolchains
* Domain specific libraries
* Many apps teams

]

???

* These teams usually consume platform team outputs
* Often deliver into each other to aid in validation
* Then they deliver on to…

---
count: false

# Scaling up - how many teams?

.col-4[
### Platform

* Toolchains
* Base OS
* Various middlewares
* Core UX facilities
* OTA updates
* …etc…
]

.col-4[
### Apps

* Consume platforms and toolchains
* Domain specific libraries
* Many apps teams

]

.col-4[
### Systems

* Integrating everything
* Deal with complex interactions
* Most complete testing scenarios
* Produce deployables
* Feedback provided to platform and apps
]

???

* Here we combine all the work
* Most complex interaction between components
* Most complete testing possible
* Deployables have to be produced and tested
* Feedback to other teams for things they could not have found alone

---
count: false

# Scaling up - how many teams?

.col-4[
### Platform

* Toolchains
* Base OS
* Various middlewares
* Core UX facilities
* OTA updates
* …etc…
]

.col-4[
### Apps

* Consume platforms and toolchains
* Domain specific libraries
* Many apps teams

]

.col-4[
### Systems

* Integrating everything
* Deal with complex interactions
* Most complete testing scenarios
* Produce deployables
* Feedback provided to platform and apps
]

## Surely there's a better way?

???

* Codethink clients are often organised like this
* We've been dealing with this for a long time
* Working on ways to improve matters
* Out of all of our work has come…

---
class: white-impact

.responsive[![BuildStream Logo](assets/BuildStream-logo-blue.png)]

### <https://buildstream.build>

???

* BuildStream is an integration tool
    * …uses any number of build systems
    * …of which we feel Bazel is perhaps the best, fastest, most correct.
* BuildStream can target the construction of software…
   * …from embedded device firmware and IoT
   * …through server operating systems
   * …including things like automotive platforms, industrial logistics and
     control, and mobile devices
* Toolchains in Bazel are one kind of thing which BuildStream deals with.
    * BuildStream is, we feel, an excellent way to construct those
	* And to provide a basis for Bazel and other tooling to construct
      further software more safely.

---
class: white-impact
count: false

.responsive[![BuildStream Logo](assets/BuildStream-logo-blue.png)]

### <https://buildstream.build>

???

* Re-do of an older **internal** project at Codethink
* Explicit goal of building a community rather than being a Codethink project.
* And of working *with* other tooling projects such as Bazel
* …rather than trying to replace/outdo other tools

<!-- -->

* BuildStream can be used as a way to **get to Bazel** when perhaps your
  software stack does not lend itself to being fully in the Bazel world.

---
class: white-impact

.col-6[.center[![GNOME logo](assets/GnomeLogoVertical.svg)
https://gnome.org/
]]

.col-6[.center[![FDSDK logo](assets/MM_freedesktop-01.svg)
https://freedesktop-sdk.io/
]]

???

* Started at the end of 2016 initially to solve GNOME integration use-cases
  (jhbuild, gnome-continuous, etc)
* Now community maintained for those use-cases
* Adopted to solve a similar problem to the GNOME ones, but for base platforms,
  the Freedesktop SDK project moved from Yocto/flatpak-builder to BuildStream
* Freedesktop SDK provides toolchains and platforms to build systems out of
  Linux and a large collection of other free software.

---
count: false
class: impact

# But can we go .big[bigger?]

???

* GNOME + FDSDK circa 700 to 800
* Client impressed at GUADEC, invited us to solve their use-case
* Team, including me, focus on performance / scaling up
* Now used in integrations of 100s of 1000s of components
* Full gamut of build systems including Bazel
* And by adopting technology such as CAS and REAPI
* integrates with a horizontally scalable Bazel compatible infrastructure

---

# BuildStream - Project Shape


???

* Look at BuildStream project
* Each piece of software as an integral component
* Focus on interdependencies and integration

---
count: false

# BuildStream - Project Shape

* Project configuration

???

* `project.conf` YAML fundamentals
    * project name
    * where other inputs are
    * configuration options
* Like `WORKSPACE` - all understanding of the software stems from here

---
count: false

# BuildStream - Project Shape

* Project configuration
* Elements and Sources

???

* Fundamentally, elements and sources
* Elements: integral piece of work to be done
    * such as a software build
    * or an integration step.
* Sources: sets of inputs
    * staged together as input to such a process

---
count: false

# BuildStream - Project Shape

* Project configuration
* Elements and Sources
* Dependencies


???

* Elements form a directed graph
* Dependencies are arcs in that graph
    * A build-deps B build-deps C, means C builds, then B, then A
* Deps can be runime or buildtime or both, so arcs are annotated
* Different operations follow different arc kinds at different times

---
count: false

# BuildStream - Project Shape

* Project configuration
* Elements and Sources
* Dependencies
* <strike>Turtles</strike> Elements all the way down

???

* As said before…
* …in addition to elements representing singular integral pieces of software,
  elements also represent integrations of software

---
count: false

# BuildStream - Project Shape

* Project configuration
* Elements and Sources
* Dependencies
* <strike>Turtles</strike> Elements all the way down
* Joining BuildStream projects together

???

* Need to join projects together, like how Bazel offers remote repositories
* BuildStream has this.
* Called 'Junction' elements and they are semi-special in that they
  are handled by the loader rather than the main build machinery.
* Again, a little like a repository rule in bazel.
* Staged sources then contain a buildstream project
* That project's elements are then made available in the containing project,
* Like how the labels of another repository can be made available in a Bazel
  workspace.

---
title: libjpeg.bst

```yaml
kind: cmake

depends:
- filename: bootstrap-import.bst
- filename: public-stacks/buildsystem-cmake.bst
  type: build
- filename: components/nasm.bst
  type: build

# ... bits elided ...

sources:
- kind: git_tag
  url: github:libjpeg-turbo/libjpeg-turbo.git
  track: master
  ref: "2.0.3-0-g5db6a6819d0f904e0b58f34ae928fea234adb1a0"
```

???

* Consider cmake element
* From FDSDK
* They build libjpeg-turbo as a component
* Put into toolchains, platforms systems.

---
count: false
title: libjpeg.bst

```yaml
*kind: cmake

depends:
- filename: bootstrap-import.bst
- filename: public-stacks/buildsystem-cmake.bst
  type: build
- filename: components/nasm.bst
  type: build

# ... bits elided ...

sources:
*- kind: git_tag
  url: github:libjpeg-turbo/libjpeg-turbo.git
  track: master
  ref: "2.0.3-0-g5db6a6819d0f904e0b58f34ae928fea234adb1a0"
```

???

* First we see `kind` in two places
* Buildstream's behaviour comes mostly from plugins
* `kind` selects the plugin to handle the element / source

---
count: false
title: libjpeg.bst

```yaml
kind: cmake

*depends:
*- filename: bootstrap-import.bst
*- filename: public-stacks/buildsystem-cmake.bst
* type: build
*- filename: components/nasm.bst
* type: build

# ... bits elided ...

sources:
- kind: git_tag
  url: github:libjpeg-turbo/libjpeg-turbo.git
  track: master
  ref: "2.0.3-0-g5db6a6819d0f904e0b58f34ae928fea234adb1a0"
```

???

* Next up, runtime and build time dependencies.
* First is both, other two just build time
* Distinction is important.  You don't need cmake to *use* libjpeg-turbo
* Since elements/deps form a graph:
    * Element can be depended on in different ways at different points
    * Elements can be reused by many other elements
    * Since Elements can be deployment outputs, this means that one
      build can be reused in multiple contexts

---
count: false
title: libjpeg.bst

```yaml
kind: cmake

*depends:
*- filename: bootstrap-import.bst
- filename: public-stacks/buildsystem-cmake.bst
  type: build
- filename: components/nasm.bst
  type: build

# ... bits elided ...

sources:
- kind: git_tag
  url: github:libjpeg-turbo/libjpeg-turbo.git
  track: master
  ref: "2.0.3-0-g5db6a6819d0f904e0b58f34ae928fea234adb1a0"
```

???

* In FDSDK, the `bootstrap-import` is similar to a Bazel toolchain
* Is an import of a basic system capable of compiling code.
* From here, all of FDSDK bootstraps within sandboxes.

---
count: false
title: libjpeg.bst

```yaml
kind: cmake

*depends:
- filename: bootstrap-import.bst
*- filename: public-stacks/buildsystem-cmake.bst
* type: build
- filename: components/nasm.bst
  type: build

# ... bits elided ...

sources:
- kind: git_tag
  url: github:libjpeg-turbo/libjpeg-turbo.git
  track: master
  ref: "2.0.3-0-g5db6a6819d0f904e0b58f34ae928fea234adb1a0"
```

???

* Brings in `cmake`
* Also all of `cmake`'s runtime deps

---
count: false
title: libjpeg.bst

```yaml
kind: cmake

*depends:
- filename: bootstrap-import.bst
- filename: public-stacks/buildsystem-cmake.bst
  type: build
*- filename: components/nasm.bst
* type: build

# ... bits elided ...

sources:
- kind: git_tag
  url: github:libjpeg-turbo/libjpeg-turbo.git
  track: master
  ref: "2.0.3-0-g5db6a6819d0f904e0b58f34ae928fea234adb1a0"
```

???

* Adds `nasm` - presumably to build optimised code
* And of course, `nasm`'s runtime deps

---
count: false
title: libjpeg.bst

```yaml
kind: cmake

*depends:
*- filename: bootstrap-import.bst
*- filename: public-stacks/buildsystem-cmake.bst
* type: build
*- filename: components/nasm.bst
* type: build

# ... bits elided ...

sources:
- kind: git_tag
  url: github:libjpeg-turbo/libjpeg-turbo.git
  track: master
  ref: "2.0.3-0-g5db6a6819d0f904e0b58f34ae928fea234adb1a0"
```

???

* All these elements and their dependencies are tracked
* Inputs are known, thus identities of outputs can be computed
* Similar to, but distinct from, Bazel's approach

---
count: false
title: libjpeg.bst

```yaml
kind: cmake

depends:
- filename: bootstrap-import.bst
- filename: public-stacks/buildsystem-cmake.bst
  type: build
- filename: components/nasm.bst
  type: build

# ... bits elided ...

*sources:
*- kind: git_tag
* url: github:libjpeg-turbo/libjpeg-turbo.git
* track: master
* ref: "2.0.3-0-g5db6a6819d0f904e0b58f34ae928fea234adb1a0"
```

???

* Next we have the sources for the element.
* Here we only have one source

---
count: false
title: libjpeg.bst

```yaml
kind: cmake

depends:
- filename: bootstrap-import.bst
- filename: public-stacks/buildsystem-cmake.bst
  type: build
- filename: components/nasm.bst
  type: build

# ... bits elided ...

*sources:
- kind: git_tag
* url: github:libjpeg-turbo/libjpeg-turbo.git
  track: master
  ref: "2.0.3-0-g5db6a6819d0f904e0b58f34ae928fea234adb1a0"
```

???

* The source comes from the given repository -- this is using BuildStream's
  URL rewriting capability, the `github:` prefix can be rewritten in project
  configuration to allow users to have mirrored the content elsewhere.

---
count: false
title: libjpeg.bst

```yaml
kind: cmake

depends:
- filename: bootstrap-import.bst
- filename: public-stacks/buildsystem-cmake.bst
  type: build
- filename: components/nasm.bst
  type: build

# ... bits elided ...

*sources:
- kind: git_tag
  url: github:libjpeg-turbo/libjpeg-turbo.git
  track: master
* ref: "2.0.3-0-g5db6a6819d0f904e0b58f34ae928fea234adb1a0"
```

???

* `ref` means identifier of actual source
* In `git` that's a SHA, `git_tag` uses a `git describe` format
* Akin to a merkle-root for the source
* Used to identify the inputs as part of the cache key calculation
* Similar to how Bazel constructs an `Action`

---
count: false
title: libjpeg.bst

```yaml
kind: cmake

depends:
- filename: bootstrap-import.bst
- filename: public-stacks/buildsystem-cmake.bst
  type: build
- filename: components/nasm.bst
  type: build

# ... bits elided ...

*sources:
- kind: git_tag
  url: github:libjpeg-turbo/libjpeg-turbo.git
* track: master
  ref: "2.0.3-0-g5db6a6819d0f904e0b58f34ae928fea234adb1a0"
```

???

* Concept in BuildStream called "source tracking"
* Here we say "track `master` branch"
* When instructed, BuildStream fetches element source, and tells source plugin
  to track.
* For `git_tag` this implies a repository refresh, and then a check for any
  updates to the given tracking branch.
* Source plugin updates `ref` if changed.
* After all sources for an element have tracked, BuildStream rewrites the
  element file to update the refs.
* Automating this process leads to speculative update CI pipelines offering up
  validated tracking updates.
* Reduces workload on integration team tracking upstream changes.

---
class: demo

# Let's explore a project

???

* Pop to a terminal and show FDSDK, project.conf, etc.
* Explain FDSDK provide examples of integrating their work into a bootable
  system, and say that we're going to do that now.
* Fix elements/app.bst bug (mention already fixed in gitlab)
* "While we're here, let's open a workspace"
    * Give me a name
    * Edit the C label
* Run a build of `vm/image.bst`, show it running the build
* "We'll come back later and look at the result of the integration"

---

# Limits

* UNIX (Linux) hosted (works on other *NIX, maybe MacOS)

???

* BuildStream very POSIX, so works on UNIXy OSs
* Does work on WSL, remote-build-only currently due to FUSE needs
* MacOS similarly remote-only
* Will work on WSL2 we expect
* Work underway to enable more sandboxing tech to reduce these limits

---

# Limits

* UNIX (Linux) hosted (works on other *NIX, maybe MacOS)
* Primarily focussed on native build

???

* Can be used to cross-build, if crossing arch, same OS kind
* Indeed, the FDSDK project provides x86_64 to aarch64 cross toolchain
* Primary focus remains native build
* In part, due to experience that cross-building ends up needing patches
* Patches often not accepted by upstream
* This leads to expense of manual reintegration on upstream changes
* Cross-compilation toolchains as part of a project can be used (e.g. to build
  embedded controller code for a system or some such)

---

# Limits

* UNIX (Linux) hosted (works on other *NIX, maybe MacOS)
* Primarily focussed on native build
* Stringent Sandboxing limits caching

???

* Due to the way BuildStream's sandboxes are constructed and torn down
  it is very hard to reuse intermediate artifacts from a build.
* This is being worked on, to enable build systems such as Bazel to access
  their intermediate artifacts from within the sandbox.

---

# Limits

* UNIX (Linux) hosted (works on other *NIX, maybe MacOS)
* Primarily focussed on native build
* Stringent Sandboxing limits caching
* No short-circuiting on identical build outputs

???

* BuildStream names its artifacts based on the identity of the inputs
* …as such, can label its entire build graph without doing any work on outputs
* …and can know if it already has the final output very quickly

<!-- -->

* Bazel names its outputs based on their contents
* …so has to compute each output in turn (may be cached of course)
* …but can stop as soon as it detects a change no longer propagates

<!-- -->

* Both approaches have their benefits and their drawbacks, and by combining
  the two we hope to mitigate the weaknesses and amplify the strengths

---
class: impact

# Isn't this *Bazel*Con?

???

* Let's bring Bazel back into the picture…
* It's why we're here, after all…
* BuildStream already shares tech…
* REAPI/CAS…
* But let's make BuildStream integrate with Bazel itself…

---
title: bazel.bst

```yaml
*kind: bazel_build

build-depends:
...
- filename: bazel-bootstrap.bst

variables:
  target: "//src:bazel"

config:
  install-commands:
  - >
    install -m 755 -D bazel-bin/src/bazel
      "%{install-root}%{bindir}/bazel-real"
  - >
    install -m 755 -D scripts/packages/bazel.sh
      "%{install-root}%{bindir}/bazel"
...
```

???

* To do that…
* …we wrote a `bazel_build` element plugin
* …which understands how to run `bazel`

---
title: bazel.bst
count: false

```yaml
kind: bazel_build

*build-depends:
*...
*- filename: bazel-bootstrap.bst

variables:
  target: "//src:bazel"

config:
  install-commands:
  - >
    install -m 755 -D bazel-bin/src/bazel
      "%{install-root}%{bindir}/bazel-real"
  - >
    install -m 755 -D scripts/packages/bazel.sh
      "%{install-root}%{bindir}/bazel"
...
```

???

* Just like for C/C++, we need a bootstrap version
* This is an import of a binary bazel release
* This lets us get a rebuild of bazel done in the sandbox

---
title: bazel.bst
count: false

```yaml
kind: bazel_build

build-depends:
...
- filename: bazel-bootstrap.bst

*variables:
* target: "//src:bazel"

config:
  install-commands:
  - >
    install -m 755 -D bazel-bin/src/bazel
      "%{install-root}%{bindir}/bazel-real"
  - >
    install -m 755 -D scripts/packages/bazel.sh
      "%{install-root}%{bindir}/bazel"
...
```

???

* Target label tells plugin what to `bazel build`

---
title: bazel.bst
count: false

```yaml
kind: bazel_build

build-depends:
...
- filename: bazel-bootstrap.bst

variables:
  target: "//src:bazel"

*config:
* install-commands:
* - >
*   install -m 755 -D bazel-bin/src/bazel
*     "%{install-root}%{bindir}/bazel-real"
* - >
*   install -m 755 -D scripts/packages/bazel.sh
*     "%{install-root}%{bindir}/bazel"
...
```

???

* BuildStream lets us set commands for configuration, build, installation, etc.
* We set the install commands to cherry-pick the bazel binary and wrapper
  script into the output path for the element.

---
title: bazel.bst

```yaml
sources:
- kind: git_tag
  url: https://github.com/bazelbuild/bazel.git
  track: "1.1.0"
  ref: "1.1.0-0-g8075057af6108ebc23c146f18eecec911d4b8c00"
- kind: patch
  path: patches/bazel-grpc-rename-gettid.patch
  strip-level: 0
- kind: local
  path: files/bazel-manifest.py
- kind: bazel_source
  repo-file: bazel-manifest.py
  ref: >
    70ccedab34708dd6192235cec408ac66c5f65d822ff4354a352986efe9ca1314
```

???

* For any bazel-built project
* Including Bazel itself
* there are a number of sources needed.

---
title: bazel.bst
count: false

```yaml
sources:
*- kind: git_tag
* url: https://github.com/bazelbuild/bazel.git
* track: "1.1.0"
* ref: "1.1.0-0-g8075057af6108ebc23c146f18eecec911d4b8c00"
*- kind: patch
* path: patches/bazel-grpc-rename-gettid.patch
* strip-level: 0
- kind: local
  path: files/bazel-manifest.py
- kind: bazel_source
  repo-file: bazel-manifest.py
  ref: >
    70ccedab34708dd6192235cec408ac66c5f65d822ff4354a352986efe9ca1314
```

???

* First the source of the thing to build.
* This is Bazel's source (1.1, though working on 1.2)
* We also include a patch to fix a name clash in `unistd.h`
* Patch comes from files in the project tree.

---
title: bazel.bst
count: false

```yaml
sources:
- kind: git_tag
  url: https://github.com/bazelbuild/bazel.git
  track: "1.1.0"
  ref: "1.1.0-0-g8075057af6108ebc23c146f18eecec911d4b8c00"
- kind: patch
  path: patches/bazel-grpc-rename-gettid.patch
  strip-level: 0
*- kind: local
* path: files/bazel-manifest.py
- kind: bazel_source
  repo-file: bazel-manifest.py
  ref: >
    70ccedab34708dd6192235cec408ac66c5f65d822ff4354a352986efe9ca1314
```

???

* This just adds a local file from the project tree into the sources for
  the element.
* This is a cquery output
* Avoidable if you *MUST*
* Host bazel can work it out
* But this way, cquery output is tracked like a `Cargo.lock` or `Gemfile.lock`
* Improving the completeness of the sandboxing for reproducibility

---
title: bazel.bst
count: false

```yaml
sources:
- kind: git_tag
  url: https://github.com/bazelbuild/bazel.git
  track: "1.1.0"
  ref: "1.1.0-0-g8075057af6108ebc23c146f18eecec911d4b8c00"
- kind: patch
  path: patches/bazel-grpc-rename-gettid.patch
  strip-level: 0
- kind: local
  path: files/bazel-manifest.py
*- kind: bazel_source
* repo-file: bazel-manifest.py
* ref: >
*   70ccedab34708dd6192235cec408ac66c5f65d822ff4354a352986efe9ca1314
```

???

* New source kind: `bazel_source`
* Consumes the cquery output
* Finds and fetches all the requisite sources
* Stages them appropriately for `--distdir` usage
* Since Bazel won't have network access from the sandbox

---
class: demo

# Looking at that in-situ

???

* Look at the bazel element in-situ,
* show the plugins from bst-plugins-experimental
* and the cquery output too from the files/ tree

Explain that as this is an element like any other, you can build on top
of it with further elements, just like we build bazel atop the bootstrap copy.

Remind people that the app we changed was a bazel built element using the
bazel we built with bazel-bootstrap.

---

# .small[Advantages to integrating Bazel with BuildStream]

???

* Two tools - similar problem space - different fundamental approach
* Designed to work with other tooling, but done so somewhat in isolation from
   one another
* Opportunities to use each to overcome any problems with the other

---
count: false

# .small[Advantages to integrating Bazel with BuildStream]

* Bazel likes to use the network a *lot*

???

* Bazel is designed to expect network access for its host operations
* Unfortunately this leads to difficulties in controlling the environment
  that Bazel runs in
* One of BuildStream's strengths is in hilighting this kind of thing thanks to
  its strict sandboxing model.
* By using BuildStream's Bazel integration, we benefit from both BuildStream's
  strong guarantees on inputs, and Bazel's strength in handling the software
  build itself.

---
count: false

# .small[Advantages to integrating Bazel with BuildStream]

* Bazel likes to use the network a *lot*
* High computational complexity of determining sources with risk of untracked
  changes

???

* When preparing the plugins for BuildStream...
* We needed to determine all inputs for a build
* Bazel uses `WORKSPACE` `.bazelrc` the environment, extra CLI args, etc.
* As such, too complex to replicate, needs Bazel to tell us what to do
* fortunately…

---
count: false

# .small[Advantages to integrating Bazel with BuildStream]

* Bazel likes to use the network a *lot*
* High computational complexity of determining sources with risk of untracked
  changes

```shell
$ bazel cquery --experimental_repository_resolved_file=.bst-sources \
  //foo:bar
```

???

* Bazel provides the facility to do this in the form of `cquery`
* By combining with BuildStream, we can…
    * …help isolate developers from the fact that this is
      experimental
    * …use BuildStream to safely track the inputs fully
    * …ensuring we will replicate the same builds in all cases

---
class: playful

# Turnabout is fair&nbsp;play

???

* That was one way integration
* BuildStream can now build Bazel based software
* What about the other way around?
* How can we use BuildStream usefully within a Bazel project?

---
class: impact

# rules_BuildStream

???

* Evolving integration effort, where:
* BuildStream makes a toolchain
* Toolchain is used to build software with Bazel
* Bazel drives all this, making easy for developer
* Being improved over time, what I'm about to show is not what we want it to be
* Will be offered up to the bazelbuild project eventually

* Consumes FDSDK work, thanks again
* Those of you familiar with rules_NixOS might recognise this approach

---
title: rules_BuildStream - WORKSPACE - Repository rule

```python
workspace(name = "rules_buildstream")
# Load the bst_element repository rule
load("@rules_buildstream//buildstream:rules.bzl", "bst_element")
 
# This is the repository containing the buildstream project which
# builds a bazel cc_toolchain
local_repository(
     name = "fdsdk_toolchain",
     path = "../bazel-toolchains-fdsdk",
)
 
# This target builds the bst project to produce the usable cc_toolchain
bst_element(
    name = "fdsdk",
    build_file = "@fdsdk_toolchain//:BUILD",
    repository = "@fdsdk_toolchain",
    element = "toolchain-complete-x86_64.bst",
)
```

???

* This is an example `WORKSPACE` file
* Not the entire thing, but representative

---
count: false
title: rules_BuildStream - WORKSPACE - Repository rule

```python
*workspace(name = "rules_buildstream")
*# Load the bst_element repository rule
*load("@rules_buildstream//buildstream:rules.bzl", "bst_element")
 
# This is the repository containing the buildstream project which
# builds a bazel cc_toolchain
local_repository(
     name = "fdsdk_toolchain",
     path = "../bazel-toolchains-fdsdk",
)
 
# This target builds the bst project to produce the usable cc_toolchain
bst_element(
    name = "fdsdk",
    build_file = "@fdsdk_toolchain//:BUILD",
    repository = "@fdsdk_toolchain",
    element = "toolchain-complete-x86_64.bst",
)
```

???

* Load the BuildStream repository rules
* Obviously needs that bazel rules file too for now

---
count: false
title: rules_BuildStream - WORKSPACE - Repository rule

```python
workspace(name = "rules_buildstream")
# Load the bst_element repository rule
load("@rules_buildstream//buildstream:rules.bzl", "bst_element")
 
*# This is the repository containing the buildstream project which
*# builds a bazel cc_toolchain
*local_repository(
*    name = "fdsdk_toolchain",
*    path = "../bazel-toolchains-fdsdk",
*)
 
# This target builds the bst project to produce the usable cc_toolchain
bst_element(
    name = "fdsdk",
    build_file = "@fdsdk_toolchain//:BUILD",
    repository = "@fdsdk_toolchain",
    element = "toolchain-complete-x86_64.bst",
)
```

???

* Set up a reference to local repo containing BuildStream project
* That project provides elements
* At least one of those elements will need to be a toolchain for our purposes
* Fortunately FDSDK has those

---
count: false
title: rules_BuildStream - WORKSPACE - Repository rule

```python
workspace(name = "rules_buildstream")
# Load the bst_element repository rule
load("@rules_buildstream//buildstream:rules.bzl", "bst_element")
 
# This is the repository containing the buildstream project which
# builds a bazel cc_toolchain
local_repository(
     name = "fdsdk_toolchain",
     path = "../bazel-toolchains-fdsdk",
)
 
*# This target builds the bst project to produce the usable cc_toolchain
*bst_element(
*   name = "fdsdk",
*   build_file = "@fdsdk_toolchain//:BUILD",
*   repository = "@fdsdk_toolchain",
*   element = "toolchain-complete-x86_64.bst",
*)
```

???

* Set up a rule to consume that project
* Build an element (toolchain)

---
title: rules_BuildStream - BUILD - Constraining the build

```python
# Our actual target
cc_binary(
    name = "hello-world",
    srcs = ["hello-world.cc"],
)
 
# This defines a set of constraints we can place on the
# toolchain we can use
platform(
    name = "linux",
    constraint_values = [
        "@bazel_tools//platforms:x86_64",
        "@bazel_tools//platforms:linux",
    ],
)
```

???

Next, we obviously need a `BUILD` file…

---
count: false
title: rules_BuildStream - BUILD - Constraining the build

```python
*# Our actual target
*cc_binary(
*   name = "hello-world",
*   srcs = ["hello-world.cc"],
*)
 
# This defines a set of constraints we can place on the
# toolchain we can use
platform(
    name = "linux",
    constraint_values = [
        "@bazel_tools//platforms:x86_64",
        "@bazel_tools//platforms:linux",
    ],
)
```

???

* In our BUILD file we specify the usual (the software to build)

---
count: false
title: rules_BuildStream - BUILD - Constraining the build

```python
# Our actual target
cc_binary(
    name = "hello-world",
    srcs = ["hello-world.cc"],
)
 
*# This defines a set of constraints we can place on the
*# toolchain we can use
*platform(
*   name = "linux",
*   constraint_values = [
*       "@bazel_tools//platforms:x86_64",
*       "@bazel_tools//platforms:linux",
*   ],
*)
```

???

* But, to ensure the toolchain is picked properly and works…
* …we define a set of constraints on the toolchain which will work for us.
* We can't pick the toolchain here though, that comes next…

---
title: rules_BuildStream - .bazelrc - Picking the toolchain

```yaml
# Choose the set of constrains which the toolchain used must meet.
build:fdsdk --host_platform=//:linux
build:fdsdk --platforms=//:linux

# Tell bazel where to find the toolchain produced by the
# fdsdk target in WORKSPACE.
build:fdsdk \
    --extra_toolchains=@fdsdk_toolchain//toolchain-x86_64:cc-toolchain
```

???

* Now we need to configure Bazel to use all this.  We could do it all on
  the command line, but that's prone to mistakes, so let's use `.bazelrc`

---
count: false
title: rules_BuildStream - .bazelrc - Picking the toolchain

```yaml
*# Choose the set of constrains which the toolchain used must meet.
*build:fdsdk --host_platform=//:linux
*build:fdsdk --platforms=//:linux

# Tell bazel where to find the toolchain produced by the
# fdsdk target in WORKSPACE.
build:fdsdk \
    --extra_toolchains=@fdsdk_toolchain//toolchain-x86_64:cc-toolchain
```

???

* We constrain the platforms according to the options we picked in the
  toolchain constraints in the BUILD file

---
count: false
title: rules_BuildStream - .bazelrc - Picking the toolchain

```yaml
# Choose the set of constrains which the toolchain used must meet.
build:fdsdk --host_platform=//:linux
build:fdsdk --platforms=//:linux

*# Tell bazel where to find the toolchain produced by the
*# fdsdk target in WORKSPACE.
*build:fdsdk \
*   --extra_toolchains=@fdsdk_toolchain//toolchain-x86_64:cc-toolchain
```

???

* We tell it that the toolchain exists and it should use it as the C/C++ one.
* Here, the `toolchain-x86_64` is the output from building the
  BuildStream element.

---
title: rules_BuildStream - Running Bazel
class: middle

.big[
```shell
$ export BAZEL_DO_NOT_DETECT_CPP_TOOLCHAIN=1
$ bazel --config=fdsdk build //:hello-world
```
]

???

* Of course, we need to put that together and actually run Bazel

---
count: false
title: rules_BuildStream - Running Bazel
class: middle

.big[
```shell
$ export BAZEL_DO_NOT_DETECT_CPP_TOOLCHAIN=1
*$ bazel --config=fdsdk build //:hello-world
```
]

???

* And we'll have to pass `--config=fdsdk` to pick it

---
count: false
title: rules_BuildStream - Running Bazel
class: middle

.big[
```shell
*$ export BAZEL_DO_NOT_DETECT_CPP_TOOLCHAIN=1
$ bazel --config=fdsdk build //:hello-world
```
]

???

* Also, we can set `BAZEL_DO_NOT_DETECT_CPP_TOOLCHAIN=1` in the environment
  in order to prevent host toolchain detection to be certain we're using
  the BuildStream provided toolchain

---
class: demo

# Let's see that in action

???

* Pop to a terminal and show this content on the filesystem
* Start a build without `--config=fdsdk`, show it fail
* Comment that the environment variable is already set
    * Run gcc/g++ -- doesn't work, so even if it weren't, no buildy buildy
* Then start a build, with `--config=fdsdk` which will start running
  BuildStream
* Explain what's going on
* Run the helloworld out the other side

---
class: playful

# Integration results?

???

* Did you think I'd forgotten?
* Pop back and look at the result of the last build we did.
* Copy the system to the host
* Boot the system, show the bazel helloworld running

---

# In review

???

Let's just recap where we are and what I'd like for you to take away

---
count: false

# In review

* BuildStream and Bazel occupy very similar spaces…

???

* Both tools integrate a large number of inputs
* Usually into a smaller number of outputs
* Each takes a fundamentally different approach

---
count: false

# In review

* BuildStream and Bazel occupy very similar spaces…
    * BuildStream: Powerful and coherent when you cannot control the world.

???

* Components are integral black boxes
* Component build systems are inviolate
* Strict sandboxing, with the identity of the outputs derived from the identity
  of the inputs ahead of time.
* Typically doesn't require invasive changes to a component in order to adopt
  into a BuildStream project
* BuildStream can know immediately on loading the element tree if it has
  already finished.
    * even missing intermediates do not prevent knowing that it has already
      finished
* But BuildStream can't short-circuit a build

---
count: false

# In review

* BuildStream and Bazel occupy very similar spaces…
    * BuildStream: Powerful and coherent when you cannot control the world.
    * Bazel: Rapid and convenient, when you're in control.

???

* Bazel (typically)…
* …dives inside the components
* …reasons about all the moving parts at once
* …requires rewriting the build system as Bazel rules in order to adopt into
  a Bazel project as first-class citizen
* Yes, foreign CC (and rules_buildstream) demonstrate this isn't always
  necessary
* But by doing this, Bazel gets a very deep understanding of the build graph.
* By naming its outputs after their content, rather than their inputs:
    * Bazel can short-circuit a build,
    * But may have to reason the whole tree forward to know nothing needs
      to be done.
    * And missing intermediates may have to be computed in order to proceed

---
count: false

# In review

* BuildStream and Bazel occupy very similar spaces…
    * BuildStream: Powerful and coherent when you cannot control the world.
    * Bazel: Rapid and convenient, when you're in control.

### Bazel and BuildStream working together…

???

* As we have shown though …
* …possible to make them work together.
* …giving some powerful and exciting outcomes.

<!-- -->

* We are working on bringing these two tools together…
* …and while this is still a work in progress
* …we are continuing to work on it
* …and would love to see some of you join us
* …to make this integration work better for you.

<!-- -->

* Hope you'll all agree that…

---
count: false

# In review

* BuildStream and Bazel occupy very similar spaces
    * BuildStream: Powerful and coherent when you cannot control the world.
    * Bazel: Rapid and convenient, when you're in control.

### Bazel and BuildStream working together…

.col-4[
## .center[Complementary]
]

???

* Solving problems of component integration…
* …in subtly different ways,
* …at two different levels,
* …Bazel and BuildStream complement each other quite well.

<!-- -->

* By using the two together you…
* …don't have to adopt every dependency fully into Bazel
* …reducing your maintenance burden if upstream don't want to shift.
* …but retaining the option of integrating tightly when you need to.

---
count: false

# In review

* BuildStream and Bazel occupy very similar spaces
    * BuildStream: Powerful and coherent when you cannot control the world.
    * Bazel: Rapid and convenient, when you're in control.

### Bazel and BuildStream working together…

.col-4[
## .center[Complementary]
]
.col-4[
## .center[Cached]
]

???

* Both can use REAPI CAS for caching
* …but Bazel REAPI ActionCache and BuildStream ArtifactService are somewhat
  different because of naming constraints
* …all the individual components are shared
* Work in BuildBox will lead to Bazel being able to access REAPI from the
  sandbox, making that sharing even more powerful.

<!-- -->

* Sharing work outcomes means reduced cycle times
* …both for integrators and apps developers
* …with both teams benefitting from CI/CD pipeline outputs

---
count: false

# In review

* BuildStream and Bazel occupy very similar spaces
    * BuildStream: Powerful and coherent when you cannot control the world.
    * Bazel: Rapid and convenient, when you're in control.

### Bazel and BuildStream working together…

.col-4[
## .center[Complementary]
]
.col-4[
## .center[Cached]
]
.col-4[
## .center[Coherent]
]

???

* Bazel using the same toolchains as fundamentally underly integration…
* …leads to fewer "works for me" situations
* …apps developers can be confident that their validations hold during
  integration
* …integrators can be confident that the apps will behave properly in-situ
* …deployment teams can be assured the integration will likely succeed

---
class: demo

.background-images[![BuildStream Logo](assets/BuildStream-logo-emblem-blue.png)![Bazel Logo](assets/Bazel_logo.svg)]

# .shine-dim[Bazel and BuildStream]
## .shine[Complementary, Cached, Coherent]

<br />

### https://tinyurl.com/bazel-buildstream

???

**Important**: Encourage people to read the wiki page

---
class: demo
count: false

.col-6[<img src="assets/tinyurl-qrcode.svg" width="100%" />]
.col-6[
## .big[.shine-dim[Bazel and BuildStream]]
## .small[Complementary, Cached, Coherent]

### .dim[https://tinyurl.com/bazel-buildstream]
]

???

**Important**: Encourage people to read the wiki page

---
exclude: true

Terminal needs white background

Use multiple desktops, not one with screen

Ensure other desktops are deleted from laptop before demo

Clunky

Fix the hilight colours some more, it's not great with the grey

